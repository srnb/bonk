import sbtcrossproject.CrossPlugin.autoImport.{crossProject, CrossType}

scalaVersion in ThisBuild := "2.11.12"

val shared = Seq(
  scalaVersion := "2.11.12",
  organization := "tf.bug",
  name         := "bonk",
  version      := "0.1.0",
  resolvers ++= Seq(
    Resolver.sonatypeRepo("releases"),
    Resolver.sonatypeRepo("snapshots")
  ),
  libraryDependencies ++= Seq(
    "com.chuusai" %%% "shapeless"   % "2.3.3",
    "org.scalameta" %%% "scalameta" % "4.0.0",
    "org.scalatest" %%% "scalatest" % "3.2.0-SNAP10" % "test"
  )
)

lazy val bonk = crossProject(JSPlatform, JVMPlatform, NativePlatform)
  .crossType(CrossType.Full)
  .in(file("."))
  .settings(shared)
  .jsSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
  .jvmSettings(crossScalaVersions := Seq("2.11.12", "2.12.7"))
  .nativeSettings(crossScalaVersions := Seq("2.11.12"))

lazy val bonkJS = bonk.js
lazy val bonkJVM = bonk.jvm
lazy val bonkNative = bonk.native
