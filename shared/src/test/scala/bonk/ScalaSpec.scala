package bonk
import bonk.sc.Scala
import org.scalatest.WordSpec

import scala.meta._

class ScalaSpec extends WordSpec {

  "Scala" when {
    "verifying a valid script" should {
      "produce an error on an invalid script" in {
        assert(Scala.verify("3 object (", Map()).isLeft)
      }
      "produce a statement on a valid script" in {
        assertResult("3 + 5".parse[Stat].get.structure)(Scala.verify("3 + 5", Map()).right.get.structure)
      }
    }
  }

}
