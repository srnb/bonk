package bonk

/** A language environment that contains current memory or variables */
trait Environment[L <: Language[L, _, _, _], E] {

  def toUsefulMap[K, V](e: E): Map[K, V]

}
