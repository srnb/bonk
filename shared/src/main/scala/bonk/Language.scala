package bonk

/** Defines some sort of programming language or dialect */
sealed trait Language[L <: Language[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C] {

  final type ConstructedScript = C

  /** Verifies whether the script is valid in the language and returns a post-validated script */
  def verify[S, E](script: S, env: E)(
    implicit containerDef: ScriptConstructor[C, L, F, S],
    environmentDef: Environment[L, E]
  ): Either[F, ConstructedScript]

  /** Runs the script from start to finish, giving some sort of output */
  def run[E](script: ConstructedScript, env: E)(
    environmentDef: Environment[L, E]
  ): Either[F, O]

}

/** Unstructured languages just run instructions, they have no functions/subroutines/labels */
trait UnstructuredLanguage[L <: UnstructuredLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends Language[L, O, F, C]

/** A structured language defines some sort of way to enter the script arbitrarily */
sealed trait StructuredLanguage[L <: StructuredLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends Language[L, O, F, C] {

  /** Runs a script's entry point */
  def run[P, E](script: ConstructedScript, entry: P, env: E)(
    entryPointDef: EntryPoint[ConstructedScript, L, P],
    environmentDef: Environment[L, E]
  ): Either[F, O]

}

trait UntypedLanguage[L <: UntypedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends StructuredLanguage[L, O, F, C]

sealed trait TypedLanguage[L <: TypedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends StructuredLanguage[L, O, F, C] {

  type Definition <: TypeDefinition[L]
  type Transformer[T] = TypeTransformer[L, Definition, T]

  /** Gets a value from a specific entry point */
  def get[T, P, E](script: ConstructedScript, entry: P, environment: E)(
    implicit typeTransformer: Transformer[T],
    entryPointDef: EntryPoint[ConstructedScript, L, P],
    environmentDef: Environment[L, E]
  ): T

  /** Evaluates the type of a script stub */
  def typeOf[E](script: ConstructedScript, environment: E)(
    implicit environmentDef: Environment[L, E]
  ): Definition

}

trait DynamicallyTypedLanguage[L <: DynamicallyTypedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[
  F,
  L,
  O,
  C
], C]
    extends TypedLanguage[L, O, F, C]

sealed trait StaticallyTypedLanguage[L <: StaticallyTypedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[
  F,
  L,
  O,
  C
], C]
    extends TypedLanguage[L, O, F, C]

trait NonGenerifiedLanguage[L <: NonGenerifiedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends StaticallyTypedLanguage[L, O, F, C]

trait GenerifiedLanguage[L <: GenerifiedLanguage[L, O, F, C], O <: Output[O, L, F, C], F <: Failure[F, L, O, C], C]
    extends StaticallyTypedLanguage[L, O, F, C]
