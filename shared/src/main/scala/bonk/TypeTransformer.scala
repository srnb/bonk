package bonk

trait TypeTransformer[L <: TypedLanguage[L, _, _, _], D <: TypeDefinition[L], T] {

  def transform(t: D): T

}
