package bonk

trait Output[O <: Output[O, L, F, C], L <: Language[L, O, F, C], F <: Failure[F, L, O, C], C]

trait Failure[F <: Failure[F, L, O, C], L <: Language[L, O, F, C], O <: Output[O, L, F, C], C] {

  type P <: Position

}

trait Position
