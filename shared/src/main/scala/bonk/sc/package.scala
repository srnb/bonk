package bonk
import scala.meta._

package object sc {

  implicit def stringScalaScriptConstructor: ScriptConstructor[Stat, Scala, ScalaFailure, String] =
    new ScriptConstructor[Stat, Scala, ScalaFailure, String] {
      override def construct(s: String): Either[ScalaFailure, Stat] =
        s.parse[Stat].toEither.fold(_ => Left(new ScalaFailure), Right(_))
    }

  implicit def mapScalaEnvironment[K, V]: Environment[Scala, Map[K, V]] = new Environment[Scala, Map[K, V]] {
    override def toUsefulMap[K2, V2](e: Map[K, V]): Map[K2, V2] = Map() // FIXME: This is invalid and bad
  }

}
