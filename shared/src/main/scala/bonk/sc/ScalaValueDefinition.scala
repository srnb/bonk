package bonk.sc
import bonk.TypeTransformer

case class ScalaValueDefinition[T](name: String, v: T)(
  implicit transformer: TypeTransformer[Scala, ScalaTypeDefinition, T]
)
