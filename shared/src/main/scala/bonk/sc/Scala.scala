package bonk.sc

import bonk._

import scala.meta._

class Scala private () extends GenerifiedLanguage[Scala, ScalaOutput, ScalaFailure, Stat] {

  override type Definition = ScalaTypeDefinition

  /** Gets a value from a specific entry point */
  override def get[T, P, E](script: ConstructedScript, entry: P, environment: E)(
    implicit typeTransformer: Transformer[T],
    entryPointDef: EntryPoint[ConstructedScript, Scala, P],
    environmentDef: Environment[Scala, E]
  ): T = ???

  /** Evaluates the type of a script stub */
  override def typeOf[E](script: ConstructedScript, environment: E)(
    implicit environmentDef: Environment[Scala, E]
  ): Definition = ???

  /** Runs a script's entry point */
  override def run[P, E](script: ConstructedScript, entry: P, env: E)(
    entryPointDef: EntryPoint[ConstructedScript, Scala, P],
    environmentDef: Environment[Scala, E]
  ): Either[ScalaFailure, ScalaOutput] = ???

  /** Verifies whether the script is valid in the language and returns a post-validated script */
  override def verify[S, E](script: S, env: E)(
    implicit containerDef: ScriptConstructor[ConstructedScript, Scala, ScalaFailure, S],
    environmentDef: Environment[Scala, E]
  ): Either[ScalaFailure, ConstructedScript] = {
    containerDef.construct(script)
  }

  /** Runs the script from start to finish, giving some sort of output */
  override def run[E](script: ConstructedScript, env: E)(
    environmentDef: Environment[Scala, E]
  ): Either[ScalaFailure, ScalaOutput] = ???

}

object Scala extends Scala()
