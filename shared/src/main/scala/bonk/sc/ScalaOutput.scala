package bonk.sc

import bonk.{Failure, Output}
import scala.meta.Stat

class ScalaOutput extends Output[ScalaOutput, Scala, ScalaFailure, Stat] {}

class ScalaFailure extends Failure[ScalaFailure, Scala, ScalaOutput, Stat] {}
