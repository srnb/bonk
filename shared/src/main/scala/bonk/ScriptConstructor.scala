package bonk

trait ScriptConstructor[C, L <: Language[L, _, F, C], F <: Failure[F, L, _, C], S] {

  def construct(s: S): Either[F, C]

}
