import shapeless.Generic

package object bonk {

  type LanguageDefinition[L <: Language[L, _, _, _]] = Generic[L]

}
